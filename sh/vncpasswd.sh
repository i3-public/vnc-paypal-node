#!/bin/bash

if_any_vnc=$(lsof -Pni | grep Xtigervnc | grep -v LISTEN | grep :60200 | wc -l)

if [ "$if_any_vnc" == "0" ]; then
    
    rand1=$(md5sum /etc/passwd | awk {'print $1'})
    rand=$(echo -n $rand1"unix"$(date +%s) | md5sum)
    rand_vnc=${rand:2:8}
    rand_root=${rand:10:8}
    
    ( sudo echo $rand_vnc ; sudo echo $rand_vnc ; sudo echo n; ) | sudo vncpasswd >/dev/null 2>&1
    ( sudo echo $rand_root ; sudo echo $rand_root ; sudo echo n; ) | sudo passwd >/dev/null 2>&1
    
    # su root -c "{ echo '"$rand_vnc"'; echo '"$rand_vnc"'; echo n; } | vncpasswd" >/dev/null 2>&1
    # su root -c "{ echo '"$rand_root"'; echo '"$rand_root"'; echo n; } | passwd" >/dev/null 2>&1
    
    echo "pass: "$rand_vnc

else
    echo "in_use"
fi

