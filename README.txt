#
# wget-qO- https://gitlab.com/i3-public/vnc-paypal-node/-/raw/main/README.txt | bash


# 
# openvz 7

echo '' >> /etc/systemd/resolved.conf
echo 'nameserver 8.8.8.8' > /etc/resolv.conf
reboot

apt -y update && apt -y upgrade && apt -y autoremove


# 
# gui and vnc

apt -y install xfce4 autocutsel tigervnc-standalone-server

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt -y install ./google-chrome-stable_current_amd64.deb

{ echo '3360@@'; echo '3360@@'; echo n; } | vncserver

echo '
$geometry = "960x720";
$localhost = "no";
$rfbport = "60200";
$IdleTimeout = "300";
$depth = "16";
' >> /etc/tigervnc/vncserver-config-mandatory

sed -i 's/google-chrome-stable/google-chrome-stable --no-sandbox/g' /usr/share/applications/google-chrome.desktop
apt -y remove gnome-terminal* xterm desktop-file-utils

vncserver -kill :1 && vncserver


# 
# git, apache and php

apt -y install software-properties-common
add-apt-repository ppa:ondrej/php -y
apt -y install git apache2 php7.4 php7.4-common php7.4-cli php7.4-curl php7.4-mcrypt libapache2-mod-php7.4
systemctl enable --now apache2
sed -i 's/AllowOverride None/AllowOverride All/g' /etc/apache2/apache2.conf
a2enmod rewrite
systemctl restart apache2
echo "www-data ALL=NOPASSWD: ALL" >> /etc/sudoers
chown root:root /usr/bin/sudo && chmod 4755 /usr/bin/sudo

cd /var
git clone https://gitlab.com/i3-public/vnc-paypal-node.git
rm -rf www
mv vnc-paypal-node www
chmod 0440 /var/www/sh/vncpasswd.sh

# 
# cron sync in one file

echo "
@reboot   echo "nameserver 8.8.8.8" > /etc/resolv.conf
@reboot   sudo php -q /var/www/cron/boot.php
0 0 * * * sudo php -q /var/www/cron/day.php
0 * * * * sudo php -q /var/www/cron/hour.php
* * * * * sudo php -q /var/www/cron/minute.php
" > /var/spool/cron/crontabs/root


