<?php


$file = '/var/www/.env';

if( file_exists($file) ){

	$file = file($file);

	if( sizeof($file) ){

		foreach( $file as $line ){
		
			if(! $line = trim($line,"\r\t\n ") )
				continue;
				
			if( substr($line, 0, 1) == '#' )
				continue;
				
			if(! strstr($line, '=') )
				continue;

			$pos = strpos($line, '=');
			
			$k = substr($line, 0, $pos);
			$v = substr($line, $pos+1 );

			if( strtolower($v) === 'false' )
				$v = false;
			if( strtolower($v) === 'true' )
				$v = true;
			
			define($k, $v);

		}

	}

}

