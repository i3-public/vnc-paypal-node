<?php


function sync_env(){

    $name = gethostname();
    $name = explode(".", $name)[0];
    $rand = substr(md5(rand(100,999)),6,6);
    $sha1 = substr(sha1("{$name}.{$rand}"),6,6);
    $hash = $rand.$sha1;

    $json = fgct("https://xwork.app/api/feed/paypal/env/?name={$name}&hash={$hash}", 8);
    $data = json_decode($json, true);

    if( $data['status'] == 'OK' ){
        shell_exec("cd /var/www; sudo touch .env; chown www-data:www-data .env");
        file_put_contents('/var/www/.env', $data['env']);
    }

}

