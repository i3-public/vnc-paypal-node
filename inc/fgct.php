<?php

# 28 Feb 2022

function fgct( $url, $timeout=4, $error=false, $proxy=false, $user_agent="96180ef8e8512f7fd17f84ef1f683c88" ){

	if( $proxy ){
		$url = 'http://'.streamshot_random_server().'/proxy-agent/?url='.urlencode($url).'&timeout='.$timeout;
	}
	
	$contx = stream_context_create([

		'http' => [
			'timeout' => intval($timeout),
			'header' => "User-Agent: ".$user_agent."\r\n",
		],

	    'ssl' => [
	        'verify_peer'=>false,
	        'verify_peer_name'=>false,
	    ],

    ]);
	
	if( $error ){
		$c = file_get_contents( $url, false, $contx );
	
	} else {
		$c = @file_get_contents( $url, false, $contx );
	}
	
	if( substr($c, 0, 22) == "<br />\n<b>Warning</b>:" ){
		echo $c;
		return false;

	} else {
		return $c;
	}

}


