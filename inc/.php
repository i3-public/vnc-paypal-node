<?php



ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE);

$reserved_files = [ 'dotenv' ];


foreach( $reserved_files as $file )
	if( file_exists('/var/www/inc/'.$file.'.php') )
		include_once('/var/www/inc/'.$file.'.php');


foreach( glob('/var/www/inc/*.php') as $file )
	if(! in_array($file, get_included_files()) )
		include_once($file);


