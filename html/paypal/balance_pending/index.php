<?php

include '/var/www/inc/.php';

$pending = array( 'EUR' => '0.00', 'USD' => '0.00' );

$tdb = date('U') - 3600* 24 * 21; // 21 days for pending
$tmr = date('U') + 3600* 24;

$info = 'USER='.api_user
        .'&PWD='.api_pass
        .'&SIGNATURE='.api_signature
        .'&METHOD=TransactionSearch'
        .'&STARTDATE='.date('Y-m-d',$tdb).'T'.date('H:i:s',$tdb).'Z'
        .'&ENDDATE='.date('Y-m-d',$tmr).'T'.date('H:i:s',$tmr).'Z'
        .'&VERSION=94';

$curl = curl_init('https://api-3t.paypal.com/nvp');
curl_setopt($curl, CURLOPT_FAILONERROR, true);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($curl, CURLOPT_POSTFIELDS,  $info);
curl_setopt($curl, CURLOPT_HEADER, 0);
curl_setopt($curl, CURLOPT_POST, 1);

$result = curl_exec($curl);

$result = explode("&", $result);

foreach($result as $value){
    $value = explode("=", $value);
    $temp[$value[0]] = $value[1];
}

foreach($temp as $k=>$v){
    $i++;
    preg_match('#^(.*?)([0-9]+)$#is',$k,$str);
    $num=$str[2];
    $key=preg_replace('#^[A-z]_#','',$str[1]);
    if($key!=''){
        $new[$num][$key]=urldecode($v);
    }

}

if( $_GET['test'] == 1 ){
    echo "<pre>";
    var_dump($new);
    echo "</pre>";
}

foreach( $new as $tnx ){
    if( $tnx['FEEAMT'] == '0.00' and !isset($tnx['EMAIL']) and substr($tnx['TYPE'], 0, 19) != 'Currency Conversion' and $tnx['STATUS'] != 'Denied' and $tnx['TYPE'] != 'TYPE' ){
        $array_of_tnx[] = intval($tnx['NETAMT'])."_".$tnx['NAME']."_Payment";
    }
}

if( $_GET['test'] == 1 ){
    echo "<pre>";
    var_dump($array_of_tnx);
    echo "</pre>";
}

foreach( $new as $tnx ){
    if( in_array( $tnx['TYPE'], array('Payment','Payment Review') ) ){
        if( in_array($tnx['STATUS'], array('Pending','Completed','Placed', 'Cleared') ) ){
            if( $tnx['EMAIL'] == '' AND $tnx['NAME'] != '' ){
                if( $tnx['FEEAMT'] == '0.00' ){
                    if( $tnx['NETAMT'] < 0 ){
                        $pending[ $tnx['CURRENCYCODE'] ]+= abs($tnx['NETAMT']);
                    } else if( in_array( ( -1* intval($tnx['NETAMT']) )."_".$tnx['NAME']."_Payment" , $array_of_tnx ) ){
                        $pending[ $tnx['CURRENCYCODE'] ]-= abs($tnx['NETAMT']);
                    }
                }
            }

        } else if( $tnx['STATUS'] == 'Uncleared' ){
            if( $tnx['EMAIL'] != '' AND $tnx['NAME'] != '' ){
                if( $tnx['NETAMT'] == $tnx['AMT'] and $tnx['NETAMT'] > 0 ){
                    $pending[ $tnx['CURRENCYCODE'] ]+= $tnx['NETAMT'];
                }
            }
        }
    }
}


#
# remove non eur-usd balances
foreach( $pending as $curr => $amount ){
    
    if( $curr == 'PHP' ){
        $curr = 'EUR';
        $amount*= 0.01633912;
    
    } else if( $curr == 'SGD' ){
        $curr = 'EUR';
        $amount*= 0.63555292;
    
    } else if( $curr == 'GBP' ){
        $curr = 'EUR';
        $amount*= 1.13352927;
    }
    
    $amount = number_format($amount, 2, '.', '' );
    $new_pending[ $curr ]+= $amount;

}
$pending = $new_pending;
# end
#


echo json_encode($pending);



