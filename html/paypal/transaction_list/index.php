<?php

include '/var/www/inc/.php';

if(! $h = $_GET['h'] )
    $h = 48;

$tdb = date('U') - 3600* $h;
$tmr = date('U') + 3600* 24;

$info = 'USER='.api_user
        .'&PWD='.api_pass
        .'&SIGNATURE='.api_signature
        .'&METHOD=TransactionSearch'
        // .'&TRANSACTIONCLASS=REC/**/EIVED'
        .'&STARTDATE='.date('Y-m-d',$tdb).'T'.date('H:i:s',$tdb).'Z'
        .'&ENDDATE='.date('Y-m-d',$tmr).'T'.date('H:i:s',$tmr).'Z'
        .'&VERSION=94';

$curl = curl_init('https://api-3t.paypal.com/nvp');
curl_setopt($curl, CURLOPT_FAILONERROR, true);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($curl, CURLOPT_POSTFIELDS,  $info);
curl_setopt($curl, CURLOPT_HEADER, 0);
curl_setopt($curl, CURLOPT_POST, 1);

$result = curl_exec($curl);


$result = explode("&", $result);

foreach($result as $value){
    $value = explode("=", $value);
    $temp[ $value[0] ] = $value[1];
}

$new = [];
$i = 0;

foreach( $temp as $k => $v ){

    $i++;
    preg_match('#^(.*?)([0-9]+)$#is', $k, $str);

    $num = $str[2];
    $key = preg_replace('#^[A-z]_#', '', $str[1]);

    if($key!='')
        $new[$num][$key]=urldecode($v);
    
}



$arr_payment_type = [
    'Payment',
    'Purchase',
    'Recurring Payment',
    'Donation',
    'Transfer (debit)',
    'Transfer (credit)',
    'Currency Conversion (debit)',
    'Currency Conversion (credit)'
];

$arr_noMail_payment_type = [
    'Transfer (debit)',
    'Transfer (credit)',
    'Currency Conversion (debit)',
    'Currency Conversion (credit)'
];

if( !is_array($new) or !sizeof($new) ){
    $tnx_arr = [];

} else foreach( $new as $tnx ){

    if( 
        ($tnx['AMT']<0 and $tnx['TYPE']=='Authorization')
        or
        in_array( $tnx['TYPE'], $arr_payment_type )
    ){

        if(! $tnx['EMAIL'] ){

            if( $tnx['TYPE'] == 'Purchase' AND $tnx['AMT'] < 0 ){
                $tnx['EMAIL'] = $tnx['NAME'];

            } else if( in_array($tnx['TYPE'], $arr_noMail_payment_type) ){
                // 

            } else {
                continue;
            }

        }

        // if( $tnx['EMAIL'] != '' ){
            $tnx_arr[] = array( 
                
                'TYPE'         => $tnx['TYPE'],
                'TIMESTAMP'    => $tnx['TIMESTAMP'], 
                'EMAIL'        => $tnx['EMAIL'], 
                'TRANSACTIONID'=> $tnx['TRANSACTIONID'], 
                'AMT'          => $tnx['AMT'], 
                'CURRENCYCODE' => $tnx['CURRENCYCODE'],
                'FEE'          => $tnx['FEEAMT'],
                'STATUS'       => $tnx['STATUS'],

            );
        // }
    }
}

header("Content-Type: application/json; charset=UTF-8");
echo json_encode($tnx_arr);

