<?php

include '/var/www/inc/.php';
include 'paypal.class.php';


if( !defined('api_user') or api_user == '' ){
    echo '0.00 N/A';
    die();
}


# else

$paypal = new Paypal( api_user, api_pass, api_signature );
$r = $paypal->call('GetBalance', array('RETURNALLCURRENCIES' => 1));


// $arrContextOptions = array( "ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false ) );
// $ex_rate = file_get_contents( 'http://api.exchangeratesapi.io/latest', false, stream_context_create($arrContextOptions) );

$ex_rate = '{"rates":{"CAD":1.516,"HKD":9.2695,"ISK":152.9,"PHP":58.048,"DKK":7.4363,"HUF":366.29,"CZK":26.303,"AUD":1.5565,"RON":4.8813,"SEK":10.1863,"IDR":17184.09,"INR":87.2305,"BRL":6.7979,"RUB":88.8807,"HRK":7.5745,"JPY":129.3,"THB":36.422,"CHF":1.1066,"SGD":1.6008,"PLN":4.5748,"BGN":1.9558,"TRY":8.9502,"CNY":7.7489,"NOK":10.211,"NZD":1.6737,"ZAR":18.2619,"USD":1.1938,"MXN":25.3204,"ILS":3.9606,"GBP":0.863,"KRW":1347.11,"MYR":4.8635},"base":"EUR","date":"2021-03-05"}';

$ex_rate = json_decode($ex_rate, true);
$ex_rate = $ex_rate['rates'];
$ex_rate['EUR'] = 1;

// var_dump($r);

for( $i=0; $i<=20; $i++ ){
    
    if(! isset($r['L_CURRENCYCODE'.$i]) ){
        break;
    }
    
    if( $ex_rate_this = $ex_rate[ $r['L_CURRENCYCODE'.$i] ] ){
        $total+= floatval( $r['L_AMT'.$i] * ( $r['L_CURRENCYCODE'.$i] == 'EUR' ? 1 : 0.9669 ) / $ex_rate_this );
    }

}

foreach( $r as $k0 => $r0 ){
    if( substr($k0, 0, 14) == 'L_CURRENCYCODE' ){
        $the_i = substr($k0, 14);
        if( !$eur and $r0 == 'EUR'){
            $eur = $r['L_AMT'.$the_i];

        } else if( !$usd and $r0 == 'USD' ){
            $usd = $r['L_AMT'.$the_i];
        }
    }
}


if( !$eur or !$usd ){
	$usd = $r['L_AMTUSD'] ? : 0;
	$eur = $total - $ex_rate[ $usd ];
}


$total = number_format($total, 2, '.', '');

if( isset($_GET['xml']) ){
    header ("Content-Type:text/xml");
    echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
    echo "<items>\n";
    echo "<balance>". $total ."</balance>\n";
    echo "<currency>EUR</currency>\n";
    echo "<date>". gmdate("H:i:s") ."</date>\n";
    echo "</items>\n";

} else if( $r['L_CURRENCYCODE0'] == '' ) {
    echo $r['L_LONGMESSAGE0'];
    
} else {

    // echo $total." EUR";
    echo json_encode([ 'total'=>$total, 'eur'=>$eur, 'usd'=>$usd ]);

}





